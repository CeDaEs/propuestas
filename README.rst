==========
Propuestas
==========
-------------------------------------------------------------
Repositorio para propuestas para los Centros de Datos Escuela
-------------------------------------------------------------


Descripción
===========
Aquí podemos reunir, organizar, editar y clasificar las propuestas que surjan con respecto a los CeDaEs. Para hacerlo, debemos usar
el formato de texto: ResTructured Text; documentado en:

https://docutils.sourceforge.io/docs/

Requerimientos
==============
* Archivo de texto plano; con extensión `.rst`.
* Terminaciones de línea tipo unix (\r).
* Conjunto de caracteres UTF-8.
* Las imágenes deben estar en formato PNG o JPEG.

