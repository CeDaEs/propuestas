============================================================
Propuesta de contenido temático para contenedores en la nube
============================================================

Temario
=======
#. Diferencia entre Contendores Dokers, Kubernetes y VXC.
#. Como utilizar comandos intermedios en Docker (exec, vim, network, etc.).
#. Uso y creación de ambientes en Docker (Dockerfiles, Multicontenedores, Docker compose, etc.).
#. Gestión de PODs y Clusters.
#. Troubleshooting en Docker (versiones o compatibilidad).

Requisitos básicos
==================
* Conocer la página dockers.com.
* Saber sobre Linux containers.
* Saber instalar Docker para el sistema operativo Linux correspondiente (Enterprise Linux y CentOS Stream), a través de Install
  Docker Engine (apt -get update y apt -get install docker-ce docker-ce-cli containerd.io).
* Saber los siguientes conceptos básicos de dockers desktop:

.. figure:: imágenes/container_applications.png
    :alt: diagrama mostrando la pila que docker utiliza para ejecutar aplicaciones.

* Saber los siguientes comandos básicos de dockers y sus atributos:
    * Docker run
    * Docker pull
    * Docker images
    * Docker ps
    * Docker start
    * Docker stop
    * Docker logs
* Saber como construir una imagen de dockers
* Saber como ejecutar una imagen de dockers
* Saber como publicar una imagen de dockers

